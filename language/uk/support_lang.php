<?php

$lang['support_app_description'] = 'Параметри та ресурси для підтримки вашої системи. Якщо ви хочете обговорити свої вимоги, <a href="#" class="support-contact">зв’яжіться з нами</a>.';
$lang['support_app_name'] = 'Підтримка';
$lang['support_chat_not_available'] = 'Підтримка через чат наразі недоступна.';
$lang['support_community_forums'] = 'Форуми спільноти';
$lang['support_contact_upgrades'] = 'Зв’яжіться з нашою <a href="#" class="support-contact">командою оновлення</a>, щоб дізнатися більше.';
$lang['support_day_remaining'] = 'Залишився день';
$lang['support_days_remaining'] = 'Днів, що залишилися';
$lang['support_documentation'] = 'Документація';
$lang['support_knowledge_base'] = 'Розширена база знань';
$lang['support_learn_more'] = 'Вчи більше';
$lang['support_no_open_tickets'] = 'Нет открытых тикетов';
$lang['support_not_available'] = 'Нет в наличии';
$lang['support_phone'] = 'Телефон';
$lang['support_please_open_ticket'] = 'Перед началом сеанса чата откройте тикет в службу поддержки.';
$lang['support_realtime_chat'] = 'Поддержка в чате в реальном времени';
$lang['support_submit_bug_report'] = 'Отправить отчет об ошибке';
$lang['support_submit_ticket'] = 'Отправить запрос в службу поддержки';
$lang['support_upgrades_and_per_incident'] = 'Чтобы получить право на поддержку, приобретите билет для каждого инцидента или повысьте уровень своей подписки <a href="#" class="support-upgrade-url">здесь</a>.';
$lang['support_upgrade'] = 'Оновлення';
$lang['support_upgrade_window'] = 'Придатність до оновлення';
$lang['support_web'] = 'Web';
